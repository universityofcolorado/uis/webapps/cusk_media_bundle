# Module - Media Bundle

Enabling the CU Media Bundle adds Drupal Media, Media Library and Responsive Images. It also adds permissions for base roles.